class RobotSimulation():
	valid_commands = {"PLACE": None, "MOVE": None, "RIGHT": None , \
		"LEFT": None, "REPORT": None}
	direction = ["NORTH", "EAST", "SOUTH", "WEST"]
	current_direction = ""
	current_coordinates = [0,0]
	board = []

	def __init__(self, x=0, y=0, d="NORTH"):
		"""
		Simulation constructor.
		Keyword arguments:
			x -- Initial x coordinate.
			y -- Initial y coordinate.
			d -- Intial direction.
		"""
		self.board = [list("XXXXX") for x in range(0,5)]
		self.length = len(self.board)
		self.width = len(self.board[0])

		if d in self.direction:
			self.current_direction = d
		else:
			self.current_direction = "NORTH"

		if (x > self.length - 1) or (x < 0):
			self.current_coordinates[0] = 4
		else:
			self.current_coordinates[0] = 4 - x

		if (y > self.width - 1) or y < 0:
			self.current_coordinates[1] = 0
		else:
			self.current_coordinates[1] = y
		
		x_curr = self.current_coordinates[0]
		y_curr = self.current_coordinates[1]
		self.board[x_curr][y_curr] = "R"
		
		
	def print_board(self):
		"""
		Prints board state.
		"""
		for i in range(0, len(self.board)):
			print("".join(self.board[i]))
		print("-----")

	def move(self):
		"""
		Moves robot a tile in the current direction.
		Ignores movement outside the board's length and width.
		"""

		if self.current_direction == "NORTH":
			if ((self.current_coordinates[0] - 1) >= 0):
				self.board[self.current_coordinates[0]][self.current_coordinates[1]] = "X"
				self.board[self.current_coordinates[0] - 1][self.current_coordinates[1]] = "R"
				self.current_coordinates[0] -= 1
				return True
		elif self.current_direction == "EAST":
			if ((self.current_coordinates[1] + 1) <= self.width - 1):
				self.board[self.current_coordinates[0]][self.current_coordinates[1]] = "X"
				self.board[self.current_coordinates[0]][self.current_coordinates[1] + 1] = "R"
				self.current_coordinates[1] += 1
				return True
		elif self.current_direction == "SOUTH":
			if ((self.current_coordinates[0] + 1) <= self.length - 1):
				self.board[self.current_coordinates[0]][self.current_coordinates[1]] = "X"
				self.board[self.current_coordinates[0] + 1][self.current_coordinates[1]] = "R"
				self.current_coordinates[0] += 1
				return True
		else:
			if ((self.current_coordinates[1] - 1) >= 0):
				self.board[self.current_coordinates[0]][self.current_coordinates[1]] = "X"
				self.board[self.current_coordinates[0]][self.current_coordinates[1] - 1] = "R"
				self.current_coordinates[1] -= 1
				return True
		return False

	def report(self):
		"""
		Prints out current location and direction.
		"""
		print("The robot is at %i,%i and is facing %s" % \
			(self.current_coordinates[1], ((self.length - 1) - \
				self.current_coordinates[0]), self.current_direction))

	def left(self):
		"""
		Changes robot direction counter clockwise.
		"""
		idx = self.direction.index(self.current_direction)
		self.current_direction = self.direction[idx - 1]
		return self.current_direction


	def right(self):
		"""
		Changes robot direction clockwise.
		"""
		idx = self.direction.index(self.current_direction)
		self.current_direction = self.direction[(idx + 1) % 4]
		return self.current_direction
	
	def place(self, x = 0, y = 0, d = "NORTH"):
		"""
		Places robot R in coordinates (x,y) facing direction d
		Keyword arguments:
			x -- X coordinate (0 <= x <= board.len)
			y -- Y coordinate (0 <= y <= board[0].len)
			d -- direction (must be a value in direction)
		"""
		if (x >= 0  and x <= self.width) and (y >= 0 and y <= self.length) \
			and (d in self.direction):
			curr_x = self.current_coordinates[0]
			curr_y = self.current_coordinates[1]
			self.board[curr_x][curr_y] = "X"
			self.current_coordinates[0] = 4 - x
			self.current_coordinates[1] = y
			self.board[4 - x][y] = "R"
			self.current_direction = d;
			return True
		return False

	
	def validate_file(self, file=None):
		"""
		Checks if all commands in a given file are valid.
		Keyword arguments:
			file -- file to check
		"""
		is_valid = False
		errors = []
		if file == None:
			return (errors, is_valid)
		else:
			try:
				line_number = 1
				for line in file:
					cmd = line.rstrip("\n").split(" ")
					if cmd[0] not in self.valid_commands:
						errors.append("Line %i: %s\n" % (line_number, cmd))
					line_number += 1
				if len(errors) == 0:
					is_valid = True
				return (errors,is_valid)
			except IOError:
				print("File does not exist")
				return (errors, is_valid)
				



robot = RobotSimulation()
p = "Please enter the filename of the sample input or \
enter QUIT to stop the simulation: "
file_name_prompt = "Please input "

while True:
	i = input(p)
	if i == "QUIT":
		print("Simulation stopped.")
		break
	else:
		try:
			file = open(i, "r")
			is_valid = robot.validate_file(file)
			file.close()
			if not is_valid[1]:
				print("File is invalid. Following errors were found: ")
				for i in range(0, len(is_valid[0])):
					print(" - " + is_valid[0][i])
			else:
				file = open(i, 'r')
				for l in file:
					cmd = l.rstrip("\n").split(" ")
					if cmd[0] == "MOVE":
						robot.move()
					elif cmd[0] == "LEFT":
						robot.left()
					elif cmd[0] == "RIGHT":
						robot.right()
					elif cmd[0] == "PLACE":
						params = cmd[1].split(",")
						robot.place(int(params[1]),int(params[0]), params[2])
					else:
						robot.report()
				file.close()
				print("Simulation done.")
			robot.place(0,0,"NORTH")
		except IOError:
			print("File does not exist.")
