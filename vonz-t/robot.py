'''
Toy Robot Simulator
ABOUT: A robot simulation on a 5x5 tile.
'''   

#function that handles the custom location of the robot
def place(x,y,f):
    print x, y
    if x > 5 or y > 5:
        print "Out of bounds, ignoring command"
        return x,y,f
    else:
        current_x_pos = x
        current_y_pos = y
        current_face = f

        return current_x_pos, current_y_pos, current_face

#function makes the robot move one tile
def move(x,y,f):
    if f == "NORTH":
        y += 1
        if y > 5: #check if out of bounds after execution
            y -= 1
    if f == "SOUTH":
        y -= 1
        if y < 0: #check if out of bounds after execution
            y += 1
    if f == "EAST":
        x += 1
        if x > 5: #check if out of bounds after execution
            x -= 1
    if f == "WEST":
        x -= 1
        if x < 0: #check if out of bounds after execution
            x += 1

    return x, y

#rotate face to left or counter clockwise
def left(f):
    #check current facec and rotate counter clockwise
    if f == "NORTH":
        current_face = "WEST"
    if f == "SOUTH":
        current_face = "EAST"
    if f == "EAST":
        current_face = "NORTH"
    if f == "WEST":
        current_face = "SOUTH"

    return current_face

#rotate face to right or clockwise
def right(f):
    #check current facec and rotate clockwise
    if f == "NORTH":
        current_face = "EAST"
    if f == "SOUTH":
        current_face = "WEST"
    if f == "EAST":
        current_face = "SOUTH"
    if f == "WEST":
        current_face = "NORTH"

    return current_face

#get current position of robot
def report(x,y,f):
    #print current position
    print "***************************"
    print "Current Position:\n" +str(x) +"," +str(y) +"," +str(f)
    print "***************************"

#main method that handles init, input and function calls
def main():
    current_x_pos = 0
    current_y_pos = 0
    current_face = "NORTH"

    print "***************************"
    print "COMMANDS:"
    print "PLACE x,y,f - jump robot to a position and set face"
    print "MOVE - move on one tile to current face"
    print "LEFT - rotate counter clockwise"
    print "RIGHT - rotate clockwise"
    print "REPORT - get current position and face"
    print "QUIT - terminate"
    print "***************************"
    while True:
        command = raw_input("Enter Command:\n")
        if "PLACE" in command:
            command = command.replace("PLACE ", "")
            command_x_pos, command_y_pos, command_face = command.split(",")
            current_x_pos, current_y_pos, current_face =  place(int(command_x_pos), int(command_y_pos), command_face)
        elif "MOVE" in command:
            current_x_pos, current_y_pos = move(int(current_x_pos), int(current_y_pos), current_face)
        elif "LEFT" in command:
            current_face = left(current_face)
        elif "RIGHT" in command:
            current_face = right(current_face)
        elif "REPORT" in command:
            report(current_x_pos, current_y_pos, current_face)
        elif "QUIT" in command:
            break
        else:
            print "Command unknown"

#execute
main()

